// from csp2-ecommerce
import bcrypt from 'bcryptjs';

const users = [
  {
    //   "_id": "62a9ccee3d2274f62f91cb8b",
    name: 'Imogen Larsen',
    email: 'imogenlarsen@email.com',
    password: bcrypt.hashSync('imogen123', 10),
    isAdmin: true,
    //   "createdAt": "2022-06-15T12:13:34.861Z",
    //   "updatedAt": "2022-06-16T11:57:02.426Z",
    //   "__v": 0
  },
  {
    //   "_id": "62a9cd4c3d2274f62f91cb8d",
    name: 'Liliana Donovan',
    email: 'lilianadonovan@email.com',
    password: bcrypt.hashSync('liliana123', 10),
    isAdmin: false,
    //   "createdAt": "2022-06-15T12:15:08.037Z",
    //   "updatedAt": "2022-06-20T12:25:47.843Z",
    //   "__v": 0
  },
  {
    //   "_id": "62a9cd853d2274f62f91cb8f",
    name: 'Arlene Parks',
    email: 'arleneparks@email.com',
    password: bcrypt.hashSync('arlene123', 10),
    isAdmin: true,
    //   "createdAt": "2022-06-15T12:16:05.553Z",
    //   "updatedAt": "2022-06-15T12:16:05.553Z",
    //   "__v": 0
  },
  {
    //   "_id": "62ac5160900b62eb778fb8ed",
    name: 'Siobhan Rocha',
    email: 'siobhanrocha@email.com',
    password: bcrypt.hashSync('siobhan123', 10),
    isAdmin: false,
    //   "createdAt": "2022-06-17T10:03:12.386Z",
    //   "updatedAt": "2022-06-17T10:03:12.386Z",
    //   "__v": 0
  },
  {
    //   "_id": "62ad5d15f165d6fc79cfba7a",
    name: 'Dev Butler',
    email: 'devbutler@email.com',
    password: bcrypt.hashSync('dev123', 10),
    isAdmin: true,
    //   "createdAt": "2022-06-18T05:05:25.502Z",
    //   "updatedAt": "2022-06-18T05:06:16.211Z",
    //   "__v": 0
  },
  {
    //   "_id": "62ae62f12a9882ac68082495",
    name: 'Yvie Lin',
    email: 'yvielin@email.com',
    password: bcrypt.hashSync('yvie123', 10),
    isAdmin: true,
    //   "createdAt": "2022-06-18T23:42:41.092Z",
    //   "updatedAt": "2022-06-20T10:13:06.638Z",
    //   "__v": 0
  },
  {
    //   "_id": "62ae7f5c92a3571adadb6fa0",
    name: 'Riley Flores',
    email: 'rileyflores@email.com',
    password: bcrypt.hashSync('riley123', 10),
    isAdmin: false,
    //   "createdAt": "2022-06-19T01:43:56.229Z",
    //   "updatedAt": "2022-06-19T01:43:56.229Z",
    //   "__v": 0
  },
  {
    //   "_id": "62ae87ac73a030e06d334d8b",
    name: 'Arielle Spence',
    email: 'ariellespence@email.com',
    password: bcrypt.hashSync('arielle123', 10),
    isAdmin: false,
    //   "createdAt": "2022-06-19T02:19:24.209Z",
    //   "updatedAt": "2022-06-19T02:19:24.209Z",
    //   "__v": 0
  },
  {
    //   "_id": "62b06678c103d212b395d7bc",
    name: 'Enid Bowler',
    email: 'enidbowler@email.com',
    password: bcrypt.hashSync('enid123', 10),
    isAdmin: false,
    //   "createdAt": "2022-06-20T12:22:16.335Z",
    //   "updatedAt": "2022-06-20T12:22:16.335Z",
    //   "__v": 0
  },
];

export default users;
