import express from 'express';
import asyncHandler from 'express-async-handler';
// import path from 'path';
const router = express.Router();

import multer from 'multer';
import cloudinaryES6 from 'cloudinary';
const cloudinary = cloudinaryES6.v2;
// const cloudinary = cloudinary.v2;
import { CloudinaryStorage } from 'multer-storage-cloudinary';

// const storage = multer.diskStorage({
//   destination(req, file, cb) {
//     cb(null, 'uploads/');
//   },
//   filename(req, file, cb) {
//     cb(
//       null,
//       `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
//     );
//   },
// });

const storage = new CloudinaryStorage({
  cloudinary: cloudinary,
  params: { folder: 'csp3', allowed_formats: ['jpeg', 'png', 'jpg'] },
});

// function checkFileType(file, cb) {
//   const filetypes = /jpg|jpeg|png/;
//   const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
//   const mimetype = filetypes.test(file.mimetype);

//   if (extname && mimetype) {
//     return cb(null, true);
//   } else {
//     cb('Only images can be uploaded.');
//   }
// }

const upload = multer({ storage });
// fileFilter: function (req, file, cb) {
//   checkFileType(file, cb);
// },
// });

router.post('/', upload.single('image'), (req, res) => {
  const uploadImageURL = req.file.path;
  res.send(uploadImageURL);
});

export default router;
