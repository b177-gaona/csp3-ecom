import express from 'express';
// import asyncHandler from 'express-async-handler';
const router = express.Router();
// import Product from '../models/productModel.js';
import {
  getProductById,
  getProducts,
  deleteProduct,
  createProduct,
  updateProduct,
  reviewProduct,
  topProducts,
} from '../controllers/productController.js';
import { protect, admin } from '../middleware/authMiddleware.js';

router.route('/').get(getProducts).post(protect, admin, createProduct);
router.route('/:id/reviews').post(protect, reviewProduct);
router.get('/top', topProducts);
router
  .route('/:id')
  .get(getProductById)
  .delete(protect, admin, deleteProduct)
  .put(protect, admin, updateProduct);

// Fetch all products
// route:   GET /api/products
// access:  public (no token)
// router.get(
//   '/',
//   asyncHandler(async (req, res) => {
//     const products = await Product.find({});
//     // use this for testing
//     // throw new Error('Something went wrong');
//     res.json(products);
//   })
// );

// Fetch single product
// route:   GET /api/product/:id
// access:  public (no token)
// router.get(
//   '/:id',
//   asyncHandler(async (req, res) => {
//     //   const product = products.find((p) => p._id === req.params.id);
//     // const product = await Product.findById(req.params.id);
//     //  catch((e) => false) will return false if findById throws an error
//     const product = await Product.findById(req.params.id).catch((e) => false);
//     if (product) {
//       res.json(product);
//       // console.log(product);
//     } else {
//       //   res.status(404).json({ message: 'Product not found' });
//       res.status(404);
//       throw new Error('Product not found');
//     }
//   })
// );

export default router;
