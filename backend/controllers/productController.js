import asyncHandler from 'express-async-handler';
import Product from '../models/productModel.js';

// Fetch all products
// route:   GET /api/products
// access:  public (no token)
const getProducts = asyncHandler(async (req, res) => {
  const pageSize = 8;
  const page = Number(req.query.pageNumber) || 1;
  const keyword = req.query.keyword
    ? {
        description: {
          $regex: req.query.keyword,
          $options: 'i',
        },
      }
    : {};

  const count = await Product.countDocuments({ ...keyword });
  const products = await Product.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (page - 1));
  // use this for testing
  // throw new Error('Something went wrong');
  res.json({ products, page, pages: Math.ceil(count / pageSize) });
});

// Fetch single product
// route:   GET /api/product/:id
// access:  public (no token)
const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id).catch((e) => false);
  if (product) {
    res.json(product);
    // console.log(product);
  } else {
    //   res.status(404).json({ message: 'Product not found' });
    res.status(404);
    throw new Error('Product not found');
  }
});

// Delete single product
// route:   DELETE /api/product/:id
// access:  private/admin
const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id).catch((e) => false);
  if (product) {
    // res.json(product);
    // console.log(product);
    await product.remove();
    res.json({ message: 'Product removed' });
  } else {
    //   res.status(404).json({ message: 'Product not found' });
    res.status(404);
    throw new Error('Product not found');
  }
});

// Create single product
// route:   POST /api/products
// access:  private/admin
const createProduct = asyncHandler(async (req, res) => {
  const product = new Product({
    name: 'Product name',
    price: 0,
    user: req.user._id,
    image: '/images/sample.jpg',
    brand: 'Product Brand',
    category: 'Product Category',
    description: 'Product Description',
    countInStock: 0,
  });
  const createdProduct = await product.save();
  res.status(201).json(createdProduct);
});

// Update single product
// route:   PUT /api/products/:id
// access:  private/admin
const updateProduct = asyncHandler(async (req, res) => {
  const {
    name,
    price,
    description,
    image,
    brand,
    category,
    countInStock,
    isActive,
  } = req.body;

  const product = await Product.findById(req.params.id);
  if (product) {
    product.name = name;
    product.price = price;
    product.description = description;
    product.image = image;
    product.brand = brand;
    product.category = category;
    product.countInStock = countInStock;
    product.isActive = isActive;

    const updatedProduct = await product.save();
    res.json(updatedProduct);
  } else {
    res.status(404);
    throw new Error('Product not found');
  }
});

// Create new review
// route:   POST /api/products/:id/reviews
// access:  private
const reviewProduct = asyncHandler(async (req, res) => {
  const { rating, comment } = req.body;

  const product = await Product.findById(req.params.id);
  if (product) {
    const alreadyReviewed = product.reviews.find(
      (r) => r.user.toString() === req.user._id.toString()
    );
    if (alreadyReviewed) {
      res.status(404);
      throw new Error('Product already reviewed');
    }

    const review = {
      name: req.user.name,
      rating: Number(rating),
      comment,
      user: req.user._id,
    };

    product.reviews.push(review);

    product.numReviews = product.reviews.length;

    product.rating =
      product.reviews.reduce((acc, item) => item.rating + acc, 0) /
      product.reviews.length;

    await product.save();
    res.status(201).json({ message: 'Review added' });
    // const updatedProduct = await product.save();
    // res.json(updatedProduct);
  } else {
    res.status(404);
    throw new Error('Product not found');
  }
});

// Get top-rated products
// route:   GET /api/products/top
// access:  public
const topProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({}).sort({ rating: -1 }).limit(3);
  res.json(products);
});

export {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateProduct,
  reviewProduct,
  topProducts,
};
