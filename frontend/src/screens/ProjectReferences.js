import React from 'react';
import {
  Row,
  Col,
  Table,
  Card,
  ListGroup,
  ListGroupItem,
} from 'react-bootstrap';

const ProjectReferences = () => {
  return (
    <>
      <Row>
        <ListGroup variant='flush'>
          <ListGroupItem className='active'>
            😎 Project Credits &amp; References 😎
          </ListGroupItem>
          <ListGroupItem>
            This section lists the various individuals &amp; references that
            helped me complete this challenging project.
          </ListGroupItem>
          <ListGroupItem>
            Coding and testing took around 7 days. Deployment took another 7
            days testing different platforms &amp;customizing deployments
            because of all the errors. 😥
          </ListGroupItem>
        </ListGroup>

        <ListGroup variant='flush'>
          <ListGroupItem variant='danger'>
            Not in any particular order ...
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://boodle.zuitt.co/'
              target='_blank'
              rel='noopener noreferrer'
            >
              Zuitt Bootcamp Sessions &amp; Notes
            </a>
            :&nbsp; Led by ace tech instructor, Sir Alvin &amp; also with lots
            of help from fellow-B177 classmates.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://www.youtube.com/c/TraversyMedia'
              target='_blank'
              rel='noopener noreferrer'
            >
              Traversy Media
            </a>
            :&nbsp; Which was my main reference in building this app.
          </ListGroupItem>
          <ListGroupItem>
            My daughter: For product search &amp; product descriptions
          </ListGroupItem>
          <ListGroupItem>My son: For additional images</ListGroupItem>
          <ListGroupItem>
            <a
              href='https://www.canva.com'
              target='_blank'
              rel='noopener noreferrer'
            >
              Canva
            </a>
            :&nbsp;For background image removal &amp; minor image editing.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://medium.com/geekculture/how-to-upload-images-to-cloudinary-with-a-react-app-f0dcc357999c'
              target='_blank'
              rel='noopener noreferrer'
            >
              This Medium Article
            </a>
            :&nbsp;On uploading images to Cloudinary with React.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://www.fontawesome.com'
              target='_blank'
              rel='noopener noreferrer'
            >
              Fontawesome
            </a>
            :&nbsp;For the icons on the navbar.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://bootswatch.com/vapor/'
              target='_blank'
              rel='noopener noreferrer'
            >
              Bootswatch-Vapor
            </a>
            :&nbsp;For this cyberpunk bootstrap theme.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://codesandbox.io/examples/package/react-paypal-button-v2'
              target='_blank'
              rel='noopener noreferrer'
            >
              This Codesandox Page
            </a>
            :&nbsp;For React Paypal Button examples.
          </ListGroupItem>

          <ListGroupItem>
            <a
              href='https://stackoverflow.com/questions/69351433/i-am-trying-to-host-my-react-website-on-netlify-but-there-is-a-npm-install-error'
              target='_blank'
              rel='noopener noreferrer'
            >
              This Stackoverflow Page
            </a>
            :&nbsp; For helping me understand how to work out those conflicts of
            package dependencies errors during deployment.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://reactgo.com/specify-node-version/'
              target='_blank'
              rel='noopener noreferrer'
            >
              Reactgo
            </a>
            :&nbsp; Which I used to append the package.json file with the
            required Node.js version to help mitigate the package dependency
            deployment issues.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://stackoverflow.com/questions/72490093/heroku-not-deploying-build-due-to-dependency-issue'
              target='_blank'
              rel='noopener noreferrer'
            >
              This Stackoverflow Page
            </a>
            :&nbsp; Since I wanted to set the npm version too.
          </ListGroupItem>
          <ListGroupItem>
            <a
              href='https://devcenter.heroku.com/articles/nodejs-support#customizing-the-build-process'
              target='_blank'
              rel='noopener noreferrer'
            >
              Heroku Dev Center
            </a>
            :&nbsp; Which I referenced for custom builds &amp; ultimately fixed
            the package dependency deployment issues!
          </ListGroupItem>
        </ListGroup>
      </Row>
    </>
  );
};

export default ProjectReferences;
